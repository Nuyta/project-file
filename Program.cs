﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Задача_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите строку или путь: ");
            string line = Console.ReadLine();
            string temp;
            try
            {
                using (FileStream fs = new FileStream(line, FileMode.Open))
                {                    using (StreamReader sr = new StreamReader(fs))
                    {
                        temp = sr.ReadToEnd();
                    }
                }
                using (FileStream fs = new FileStream(line, FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine();
                        sw.WriteLine("Количество слов: " + temp.Replace(Environment.NewLine, " ").Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Count().ToString());
                        sw.WriteLine("Количество букв: " + temp.Replace(Environment.NewLine, " ").Count(x => char.IsLetter(x)));
                        sw.WriteLine("Количество пробелов: " + temp.Count(x => x == ' ').ToString());
                    }
                }
            }
            catch
            {
                using (FileStream fs = new FileStream(Directory.GetCurrentDirectory() + @"\output.txt", FileMode.Create))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(line);
                        sw.WriteLine("Количество слов: " + line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Count().ToString());
                        sw.WriteLine("Количество букв: " + line.Count(x => char.IsLetter(x)));
                        sw.WriteLine("Количество пробелов: " + line.Count(x => x == ' ').ToString());
                    }
                }
            }
        }
    }
}